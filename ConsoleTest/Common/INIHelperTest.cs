﻿using HY.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleTest.Common
{
    /// <summary>
    /// INI操作简单测试，其余使用时自行测试
    /// </summary>
    public class INIHelperTest
    {
        private static INIHelper iNIHelper = new INIHelper(System.Environment.CurrentDirectory + "\\Config.ini");
        public static void WriteTest()
        {
            iNIHelper.Write("test", "key1", "value1");
            iNIHelper.WriteString("stringtest", "stringkey1", "stringvalue1");
            iNIHelper.WriteInteger("inttest", "key1", 2);
            iNIHelper.WriteBoolean("booltest", "key1", true);
        }

        public static void ReadTest()
        {
            Console.WriteLine(iNIHelper.Read("test", "key1"));
            Console.WriteLine(iNIHelper.Read("test", "key2"));
            Console.WriteLine(iNIHelper.ReadString("stringtest", "stringkey1"));
            Console.WriteLine(iNIHelper.ReadString("stringtest", "stringkey2"));
            Console.WriteLine(iNIHelper.ReadInteger("inttest", "key1"));
            Console.WriteLine(iNIHelper.ReadInteger("inttest", "key2"));
            Console.WriteLine(iNIHelper.ReadBoolean("booltest", "key1"));
            Console.WriteLine(iNIHelper.ReadBoolean("booltest", "key2"));
        }
    }
}
