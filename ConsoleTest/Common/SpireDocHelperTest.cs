﻿using HY.Common;
using System;
using System.Collections.Generic;

namespace ConsoleTest.Common
{
    public class SpireDocHelperTest
    {
        public static void ReplaceTextAndImage()
        {
            Dictionary<string, string> textDic = new Dictionary<string, string>();
            textDic.Add("#执法机构#", "执法机构");
            textDic.Add("#是否超重#", "否");
            var sourcePath = System.Environment.CurrentDirectory + "\\Resources\\SpireDoc\\";
            Dictionary<string, string> imgDic = new Dictionary<string, string>();
            imgDic.Add("#img1#", $"{sourcePath}1.jpg");
            imgDic.Add("#img2#", $"{sourcePath}2.jpg");
            imgDic.Add("#img3#", $"{sourcePath}2.jpg");
            imgDic.Add("#img4#", $"{sourcePath}1.jpg");
            SpireDocHelper.WordReplace($"{sourcePath}SpireDocTest.doc", $"{sourcePath}SpireDocTest-{DateTime.Now.ToString("HHmmss")}.doc", textDic, imgDic);
            Console.WriteLine("替换完成");
        }
    }
}
