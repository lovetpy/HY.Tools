﻿using HY.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleTest
{
    class Program
    {
        static void Main(string[] args)
        {
            //Common.INIHelperTest.WriteTest();
            //Common.INIHelperTest.ReadTest();
            //Common.SpireDocHelperTest.ReplaceTextAndImage();

            //向数据库写入自定义属性
            Log4NetHelper.Info(new Log4NetCustomInfo() { UserId = 1, OperaterType = 2, Message = "aaaaa" });
            Log4NetHelper.Info(new Log4NetCustomInfo() { UserId = 1, OperaterType = 2, Message = "bbbbb" }, Log4NetHelper.Log4NetType.Customize);
            try
            {
                throw new Exception();
            }
            catch (Exception ex)
            {
                Log4NetHelper.Info(new Log4NetCustomInfo() { UserId = 1, OperaterType = 2, Message = ex.Message }, ex);
            }

            Console.ReadKey();
        }
    }
}
