﻿/*
 INI文件结构如下：
 ;注释
[小节名]
关键字=值
说明：
1.INI文件允许有多个小节，每个小节又允许有多个关键字， "="后面是该关键字的值。
2.值的类型有三种：字符串、整型数值和布尔值。其中字符串存贮在INI文件中时没有引号，布尔真值用1表示，布尔假值用0表示。
3.注释以分号";"开头
4.小节名和键值不区分大小写
5.INI文件绝对路径如不指定，只写文件名的话，则在Windows目录中搜索文件
6.写入操作时，如INI文件不存在，会自动创建

 */
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace HY.Common
{
    /// <summary>
    /// INI文件操作类
    /// </summary>
    public class INIHelper
    {
        public string path;

        public INIHelper(string INIPath)
        {
            path = INIPath;
        }

        #region 读写INI文件相关
        /// <summary>
        /// 声明INI文件的写操作函数
        /// </summary>
        /// <param name="section">小节名</param>
        /// <param name="key">键</param>
        /// <param name="val">值</param>
        /// <param name="filePath">INI文件绝对路径</param>
        /// <returns></returns>
        [DllImport("kernel32.dll", EntryPoint = "WritePrivateProfileString", CharSet = CharSet.Ansi)]
        private static extern long WritePrivateProfileString(string section, string key, string val, string filePath);
        /// <summary>
        /// 声明INI文件的读字符串操作函数
        /// </summary>
        /// <param name="section">小节名</param>
        /// <param name="key">键</param>
        /// <param name="def">小节名或键不存在，则将该值赋给retVal</param>
        /// <param name="retVal">返回值</param>
        /// <param name="size">返回值大小</param>
        /// <param name="filePath">INI文件绝对路径</param>
        /// <returns>获取到的值的长度</returns>
        [DllImport("kernel32.dll", EntryPoint = "GetPrivateProfileString", CharSet = CharSet.Ansi)]
        private static extern int GetPrivateProfileString(string section, string key, string def, StringBuilder retVal, int size, string filePath);
        /// <summary>
        /// 声明INI文件的读数字操作函数
        /// </summary>
        /// <param name="lpApplicationName">小节名</param>
        /// <param name="lpKeyName">键</param>
        /// <param name="nDefault">小节名或键不存在的默认值</param>
        /// <param name="lpFileName">INI文件绝对路径</param>
        /// <returns>获取到的值</returns>
        [DllImport("kernel32")]
        private static extern int GetPrivateProfileInt(string lpApplicationName, string lpKeyName, int nDefault, string lpFileName);

        /// <summary>
        /// 声明INI文件的读小节名信息操作函数
        /// </summary>
        /// <param name="lpszReturnBuffer"></param>
        /// <param name="nSize"></param>
        /// <param name="filePath">INI文件绝对路径</param>
        /// <returns>获取到的小节个数</returns>
        [DllImport("kernel32.dll", EntryPoint = "GetPrivateProfileSectionNames", CharSet = CharSet.Ansi)]
        private static extern int GetPrivateProfileSectionNames(IntPtr lpszReturnBuffer, int nSize, string filePath);
        /// <summary>
        /// 获取指定节下的键
        /// </summary>
        /// <param name="lpAppName"></param>
        /// <param name="lpReturnedString"></param>
        /// <param name="nSize"></param>
        /// <param name="filePath"></param>
        /// <returns></returns>
        [DllImport("KERNEL32.DLL ", EntryPoint = "GetPrivateProfileSection", CharSet = CharSet.Ansi)]
        private static extern int GetPrivateProfileSection(string lpAppName, byte[] lpReturnedString, int nSize, string filePath);
        #endregion

        #region 读写操作（字符串）
        /// <summary>
        /// 向INI写入数据
        /// </summary>
        /// <PARAM name="Section">节点名</PARAM>
        /// <PARAM name="Key">键名</PARAM>
        /// <PARAM name="Value">值（字符串）</PARAM>
        public void Write(string Section, string Key, string Value)
        {
            WritePrivateProfileString(Section, Key, Value, path);
        }
        /// <summary>
        /// 读取INI数据
        /// </summary>
        /// <PARAM name="Section">节点名</PARAM>
        /// <PARAM name="Key">键名</PARAM>
        /// <returns>值（字符串）</returns>
        public string Read(string Section, string Key)
        {
            StringBuilder temp = new StringBuilder(255);
            int i = GetPrivateProfileString(Section, Key, "", temp, 255, path);
            return temp.ToString();
        }
        #endregion

        #region 具体类型的读写

        #region string
        /// <summary>
        /// 读取字符串
        /// </summary>
        /// <param name="sectionName"></param>
        /// <param name="keyName"></param>
        /// <param name="defaultValue" />
        /// <returns></returns>
        public string ReadString(string sectionName, string keyName, string defaultValue = "")
        {
            const int MAXSIZE = 255;
            StringBuilder temp = new StringBuilder(MAXSIZE);
            GetPrivateProfileString(sectionName, keyName, defaultValue, temp, 255, path);
            return temp.ToString();
        }

        /// <summary>
        /// 写入字符串
        /// </summary>
        /// <param name="sectionName"></param>
        /// <param name="keyName"></param>
        /// <param name="value"></param>
        public void WriteString(string sectionName, string keyName, string value)
        {
            WritePrivateProfileString(sectionName, keyName, value, path);
        }
        #endregion

        #region Int
        /// <summary>
        /// 读取INT值
        /// </summary>
        /// <param name="sectionName"></param>
        /// <param name="keyName"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public int ReadInteger(string sectionName, string keyName, int defaultValue = 0)
        {
            return GetPrivateProfileInt(sectionName, keyName, defaultValue, path);
        }
        /// <summary>
        /// 写入INT值
        /// </summary>
        /// <param name="sectionName"></param>
        /// <param name="keyName"></param>
        /// <param name="value"></param>
        public void WriteInteger(string sectionName, string keyName, int value)
        {
            WritePrivateProfileString(sectionName, keyName, value.ToString(), path);
        }
        #endregion

        #region bool
        /// <summary>
        /// 读取布尔值
        /// </summary>
        /// <param name="sectionName"></param>
        /// <param name="keyName"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public bool ReadBoolean(string sectionName, string keyName, bool defaultValue = false)
        {
            int temp = defaultValue ? 1 : 0;
            int result = GetPrivateProfileInt(sectionName, keyName, temp, path);
            return (result == 0 ? false : true);
        }
        /// <summary>
        /// 写入布尔值
        /// </summary>
        /// <param name="sectionName"></param>
        /// <param name="keyName"></param>
        /// <param name="value"></param>
        public void WriteBoolean(string sectionName, string keyName, bool value)
        {
            string temp = value ? "1 " : "0 ";
            WritePrivateProfileString(sectionName, keyName, temp, path);
        }
        #endregion

        #endregion

        #region 删除操作
        /// <summary>
        /// 删除指定项
        /// </summary>
        /// <param name="sectionName"></param>
        /// <param name="keyName"></param>
        public void DeleteKey(string sectionName, string keyName)
        {
            WritePrivateProfileString(sectionName, keyName, null, path);
        }

        /// <summary>
        /// 删除指定节下的所有项
        /// </summary>
        /// <param name="sectionName"></param>
        public void EraseSection(string sectionName)
        {
            WritePrivateProfileString(sectionName, null, null, path);
        }
        #endregion

        #region 判断节、键是否存在
        /// <summary>
        /// 指定节知否存在
        /// </summary>
        /// <param name="section"></param>
        /// <returns></returns>
        public bool ExistSection(string section)
        {
            string[] sections = null;
            GetAllSectionNames(out sections);
            if (sections != null)
            {
                foreach (var s in sections)
                {
                    if (s == section)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        /// <summary>
        /// 指定节下的键是否存在
        /// </summary>
        /// <param name="section"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public bool ExistKey(string section, string key)
        {
            string[] keys = null;
            GetAllKeys(section, out keys);
            if (keys != null)
            {
                foreach (var s in keys)
                {
                    if (s == key)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        #endregion

        #region 配置节信息
        /// <summary>
        /// 读取一个ini里面所有的节
        /// </summary>
        /// <param name="sections"></param>
        /// <returns>-1:没有节信息，0:正常</returns>
        public int GetAllSectionNames(out string[] sections)
        {
            int MAX_BUFFER = 32767;
            IntPtr pReturnedString = Marshal.AllocCoTaskMem(MAX_BUFFER);
            int bytesReturned = GetPrivateProfileSectionNames(pReturnedString, MAX_BUFFER, path);
            if (bytesReturned == 0)
            {
                sections = null;
                return -1;
            }
            string local = Marshal.PtrToStringAnsi(pReturnedString, (int)bytesReturned).ToString();
            Marshal.FreeCoTaskMem(pReturnedString);
            //use of Substring below removes terminating null for split
            sections = local.Substring(0, local.Length - 1).Split('\0');
            return 0;
        }
        /// <summary>
        /// 返回指定配置文件下的节名称列表
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public List<string> GetAllSectionNames()
        {
            List<string> sectionList = new List<string>();
            int MAX_BUFFER = 32767;
            IntPtr pReturnedString = Marshal.AllocCoTaskMem(MAX_BUFFER);
            int bytesReturned = GetPrivateProfileSectionNames(pReturnedString, MAX_BUFFER, path);
            if (bytesReturned != 0)
            {
                string local = Marshal.PtrToStringAnsi(pReturnedString, (int)bytesReturned).ToString();
                Marshal.FreeCoTaskMem(pReturnedString);
                sectionList.AddRange(local.Substring(0, local.Length - 1).Split('\0'));
            }
            return sectionList;
        }

        /// <summary>
        /// 得到某个节点下面所有的key和value组合
        /// </summary>
        /// <param name="section">指定的节名称</param>
        /// <param name="keys">Key数组</param>
        /// <param name="values">Value数组</param>
        /// <returns></returns>
        public int GetAllKeyValues(string section, out string[] keys, out string[] values)
        {
            byte[] b = new byte[65535];//配置节下的所有信息
            GetPrivateProfileSection(section, b, b.Length, path);
            string s = System.Text.Encoding.Default.GetString(b);//配置信息
            string[] tmp = s.Split((char)0);//Key\Value信息
            List<string> result = new List<string>();
            foreach (string r in tmp)
            {
                if (r != string.Empty)
                    result.Add(r);
            }
            keys = new string[result.Count];
            values = new string[result.Count];
            for (int i = 0; i < result.Count; i++)
            {
                string[] item = result[i].Split(new char[] { '=' });//Key=Value格式的配置信息
                //Value字符串中含有=的处理，
                //一、Value加""，先对""处理
                //二、Key后续的都为Value
                if (item.Length > 2)
                {
                    keys[i] = item[0].Trim();
                    values[i] = result[i].Substring(keys[i].Length + 1);
                }
                if (item.Length == 2)//Key=Value
                {
                    keys[i] = item[0].Trim();
                    values[i] = item[1].Trim();
                }
                else if (item.Length == 1)//Key=
                {
                    keys[i] = item[0].Trim();
                    values[i] = "";
                }
                else if (item.Length == 0)
                {
                    keys[i] = "";
                    values[i] = "";
                }
            }
            return 0;
        }
        /// <summary>
        /// 得到某个节点下面所有的key
        /// </summary>
        /// <param name="section">指定的节名称</param>
        /// <param name="keys">Key数组</param>
        /// <returns></returns>
        public int GetAllKeys(string section, out string[] keys)
        {
            byte[] b = new byte[65535];

            GetPrivateProfileSection(section, b, b.Length, path);
            string s = System.Text.Encoding.Default.GetString(b);
            string[] tmp = s.Split((char)0);
            ArrayList result = new ArrayList();
            foreach (string r in tmp)
            {
                if (r != string.Empty)
                    result.Add(r);
            }
            keys = new string[result.Count];
            for (int i = 0; i < result.Count; i++)
            {
                string[] item = result[i].ToString().Split(new char[] { '=' });
                if (item.Length == 2)
                {
                    keys[i] = item[0].Trim();
                }
                else if (item.Length == 1)
                {
                    keys[i] = item[0].Trim();
                }
                else if (item.Length == 0)
                {
                    keys[i] = "";
                }
            }
            return 0;
        }
        /// <summary>
        /// 获取指定节下的Key列表
        /// </summary>
        /// <param name="section">指定的节名称</param>
        /// <returns>Key列表</returns>
        public List<string> GetAllKeys(string section)
        {
            List<string> keyList = new List<string>();
            byte[] b = new byte[65535];
            GetPrivateProfileSection(section, b, b.Length, path);
            string s = System.Text.Encoding.Default.GetString(b);
            string[] tmp = s.Split((char)0);
            List<string> result = new List<string>();
            foreach (string r in tmp)
            {
                if (r != string.Empty)
                    result.Add(r);
            }
            for (int i = 0; i < result.Count; i++)
            {
                string[] item = result[i].Split(new char[] { '=' });
                if (item.Length == 2 || item.Length == 1)
                {
                    keyList.Add(item[0].Trim());
                }
                else if (item.Length == 0)
                {
                    keyList.Add(string.Empty);
                }
            }
            return keyList;
        }
        /// <summary>
        /// 获取值
        /// </summary>
        /// <param name="section"></param>
        /// <returns></returns>
        public List<string> GetAllValues(string section)
        {
            List<string> keyList = new List<string>();
            byte[] b = new byte[65535];
            GetPrivateProfileSection(section, b, b.Length, path);
            string s = System.Text.Encoding.Default.GetString(b);
            string[] tmp = s.Split((char)0);
            List<string> result = new List<string>();
            foreach (string r in tmp)
            {
                if (r != string.Empty)
                    result.Add(r);
            }
            for (int i = 0; i < result.Count; i++)
            {
                string[] item = result[i].Split(new char[] { '=' });
                if (item.Length == 2 || item.Length == 1)
                {
                    keyList.Add(item[1].Trim());
                }
                else if (item.Length == 0)
                {
                    keyList.Add(string.Empty);
                }
            }
            return keyList;
        }

        #endregion

        #region 通过值查找键（一个节中的键唯一，可能存在多个键值相同，因此反查的结果可能为多个）

        /// <summary>
        /// 第一个键
        /// </summary>
        /// <param name="section"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public string GetFirstKeyByValue(string section, string value)
        {
            foreach (string key in GetAllKeys(section))
            {
                if (ReadString(section, key, "") == value)
                {
                    return key;
                }
            }
            return string.Empty;
        }
        /// <summary>
        /// 所有键
        /// </summary>
        /// <param name="section"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public List<string> GetKeysByValue(string section, string value)
        {
            List<string> keys = new List<string>();
            foreach (string key in GetAllKeys(section))
            {
                if (ReadString(section, key, "") == value)
                {
                    keys.Add(key);
                }
            }
            return keys;
        }
        #endregion
        
        #region 同一Section下添加多个Key\Value
        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <param name="keyList"></param>
        /// <param name="valueList"></param>
        /// <returns></returns>
        public bool AddSectionWithKeyValues(string section, List<string> keyList, List<string> valueList)
        {
            bool bRst = true;
            //判断Section是否已经存在，如果存在，返回false
            //已经存在，则更新
            //if (GetAllSectionNames(path).Contains(section))
            //{
            //    return false;
            //}
            //判断keyList中是否有相同的Key，如果有，返回false

            //添加配置信息
            for (int i = 0; i < keyList.Count; i++)
            {
                WriteString(section, keyList[i], valueList[i]);
            }
            return bRst;
        }
        #endregion
    }
}
