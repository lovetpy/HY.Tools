﻿using log4net;
using log4net.Layout;
using log4net.Layout.Pattern;
using log4net.Core;

namespace HY.Common
{
    public class Log4NetCustomConverter : PatternLayoutConverter
    {
        protected override void Convert(System.IO.TextWriter writer, LoggingEvent loggingEvent)
        {
            var customInfo = loggingEvent.MessageObject as Log4NetCustomInfo;
            if (customInfo == null)
            {
                writer.Write("");
            }
            else
            {
                switch (this.Option.ToLower())
                {
                    case "userid":
                        writer.Write(customInfo.UserId);
                        break;
                    case "operatertype":
                        writer.Write(customInfo.OperaterType);
                        break;
                    case "message":
                        writer.Write(customInfo.Message);
                        break;
                    default:
                        writer.Write("");
                        break;
                }
            }
        }
    }
}
