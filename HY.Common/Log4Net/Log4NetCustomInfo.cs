﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HY.Common
{
    public class Log4NetCustomInfo
    {
        public int UserId { get; set; }
        public int OperaterType { get; set; }
        /// <summary>
        /// 要记录的日志信息
        /// </summary>
        public object Message { get; set; }
    }
}
