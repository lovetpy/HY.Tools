﻿using log4net;
using log4net.Layout;
using log4net.Layout.Pattern;
using log4net.Core;

namespace HY.Common
{
    public class Log4NetCustomPatternLayout: PatternLayout
    {
        public Log4NetCustomPatternLayout()
        {
            this.AddConverter("customInfo", typeof(Log4NetCustomConverter));
        }
    }
}
