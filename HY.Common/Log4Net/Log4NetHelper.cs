﻿using System;
using System.Collections.Generic;
using System.Web;
/*
 * 1.需要Configs文件夹下的log4net.config配置文件
 * 2.设置配置文件如果较新则复制
*/
namespace HY.Common
{
    /// <summary>
    /// Log4Net帮助类，避免所有需要使用Log4Net的类库，都要引用Log4Net
    /// </summary>
    public class Log4NetHelper
    {
        /// <summary>
        /// 日志类型，分文件放(需在配置文件配置相应节点)
        /// </summary>
        public enum Log4NetType
        {
            Default,
            Customize,
        }
        /// <summary>
        /// 默认日志类型
        /// </summary>
        private static Log4NetType defaultLogType = Log4NetType.Default;
        /// <summary>
        /// 日志实例字典
        /// </summary>
        private static Dictionary<string, log4net.ILog> logDic = new Dictionary<string, log4net.ILog>();
        static Log4NetHelper()
        {
            if(HttpContext.Current != null)
                log4net.Config.XmlConfigurator.Configure(new System.IO.FileInfo(HttpRuntime.BinDirectory + "Configs\\log4net.config"));
            else
                log4net.Config.XmlConfigurator.Configure(new System.IO.FileInfo(AppDomain.CurrentDomain.BaseDirectory + "Configs\\log4net.config"));

            log4net.Repository.Hierarchy.Hierarchy hierarchy = log4net.LogManager.GetRepository() as log4net.Repository.Hierarchy.Hierarchy;
            if (hierarchy != null && hierarchy.Configured)
            {
                foreach (log4net.Appender.IAppender appender in hierarchy.GetAppenders())
                {
                    if (appender is log4net.Appender.AdoNetAppender)
                    {
                        var adoNetAppender = (log4net.Appender.AdoNetAppender)appender;
                        adoNetAppender.ConnectionString = "server=.;database=HY;uid=sa;pwd=123456;timeout=300;";
                        adoNetAppender.ActivateOptions();
                    }
                }
            }


            //遍历创建所有实例
            //foreach (Log4NetType log4NetType in Enum.GetValues(typeof(Log4NetType)))
            //    logDic[log4NetType.ToString()] = log4net.LogManager.GetLogger(log4NetType.ToString());
        }
        /// <summary>
        /// 使用时再创建对应的实例
        /// </summary>
        /// <param name="log4NetType"></param>
        /// <returns></returns>
        private static log4net.ILog GetLogger(Log4NetType log4NetType)
        {
            if (logDic.ContainsKey(log4NetType.ToString()))
                return logDic[log4NetType.ToString()];
            else
            {
                log4net.ILog log= log4net.LogManager.GetLogger(log4NetType.ToString());
                logDic[log4NetType.ToString()] = log;
                return log;
            }
        }



        #region Debug
        public static void Debug(object message)
        {
            GetLogger(defaultLogType).Debug(message);
        }
        public static void Debug(object message, Exception exception)
        {
            GetLogger(defaultLogType).Debug(message, exception);
        }
        public static void Debug(object message, Log4NetType log4NetType)
        {
            GetLogger(log4NetType).Debug(message);
        }
        public static void Debug(object message, Exception exception, Log4NetType log4NetType)
        {
            GetLogger(log4NetType).Debug(message, exception);
        }
        #endregion
        
        #region Info
        public static void Info(Log4NetCustomInfo message)
        {
            GetLogger(defaultLogType).Info(message);
        }
        public static void Info(object message, Exception exception)
        {
            GetLogger(defaultLogType).Info(message, exception);
        }
        public static void Info(Log4NetCustomInfo message, Log4NetType log4NetType)
        {
            GetLogger(log4NetType).Info(message);
        }
        public static void Info(object message, Exception exception, Log4NetType log4NetType)
        {
            GetLogger(log4NetType).Info(message, exception);
        }
        #endregion

        #region Warn
        public static void Warn(object message)
        {
            GetLogger(defaultLogType).Warn(message);
        }
        public static void Warn(object message, Exception exception)
        {
            GetLogger(defaultLogType).Warn(message, exception);
        }
        public static void Warn(object message, Log4NetType log4NetType)
        {
            GetLogger(log4NetType).Warn(message);
        }
        public static void Warn(object message, Exception exception, Log4NetType log4NetType)
        {
            GetLogger(log4NetType).Warn(message, exception);
        }
        #endregion

        #region Error
        public static void Error(object message)
        {
            GetLogger(defaultLogType).Error(message);
        }
        public static void Error(object message, Exception exception)
        {
            GetLogger(defaultLogType).Error(message, exception);
        }
        public static void Error(object message, Log4NetType log4NetType)
        {
            GetLogger(log4NetType).Error(message);
        }
        public static void Error(object message, Exception exception, Log4NetType log4NetType)
        {
            GetLogger(log4NetType).Error(message, exception);
        }
        #endregion

        #region Fatal
        public static void Fatal(object message)
        {
            GetLogger(defaultLogType).Fatal(message);
        }
        public static void Fatal(object message, Exception exception)
        {
            GetLogger(defaultLogType).Fatal(message, exception);
        }
        public static void Fatal(object message, Log4NetType log4NetType)
        {
            GetLogger(log4NetType).Fatal(message);
        }
        public static void Fatal(object message, Exception exception, Log4NetType log4NetType)
        {
            GetLogger(log4NetType).Fatal(message, exception);
        }
        #endregion
    }
}
