﻿using Spire.Doc;
using Spire.Doc.Documents;
using Spire.Doc.Fields;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HY.Common
{
    /// <summary>
    /// Doc帮助类
    /// </summary>
    public class SpireDocHelper
    {
        /// <summary>
        /// word文本替换
        /// </summary>
        /// <param name="sourcePath"></param>
        /// <param name="newPath"></param>
        /// <param name="textDic"></param>
        public static bool WordReplace(string sourcePath, string newPath, Dictionary<string, string> textDic)
        {
            var doc = new Document();
            try
            {
                doc.LoadFromFile(sourcePath);
                foreach (var item in textDic)
                {
                    //匹配值，替换值，大小写敏感，全字匹配
                    doc.Replace(item.Key, item.Value, false, false);
                }
                string strPath = Path.GetDirectoryName(newPath);
                if (!Directory.Exists(strPath))
                {
                    Directory.CreateDirectory(strPath);
                }
                doc.SaveToFile(newPath);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            finally
            {
                doc.Close();
            }
        }

        /// <summary>
        /// word文本及图片替换
        /// </summary>
        /// <param name="sourcePath"></param>
        /// <param name="newPath"></param>
        /// <param name="textDic">key：原文字；value：替换文字</param>
        /// <param name="imgDic">key：原文字；value：替换图片路径</param>
        public static bool WordReplace(string sourcePath, string newPath, Dictionary<string, string> textDic, Dictionary<string, string> imgDic)
        {
            var doc = new Document();
            try
            {
                doc.LoadFromFile(sourcePath);
                //开始替换文本
                foreach (var item in textDic)
                {
                    //匹配值，替换值，大小写敏感，全字匹配
                    doc.Replace(item.Key, item.Value, false, false);
                }
                //获取单元格宽度
                var tdWidth = doc.Sections[0].Tables[0].Rows[0].Cells[0].Width;
                //开始替换图片
                foreach (var item in imgDic)
                {
                    //加载替换的图片
                    Image image = Image.FromFile(item.Value);
                    //查找文档中的指定文本内容
                    TextSelection[] selections = doc.FindAllString(item.Key, false, false);
                    if (selections is null)
                        continue;
                    int index = 0;
                    TextRange range = null;
                    //遍历文档，移除文本内容，插入图片
                    foreach (TextSelection selection in selections)
                    {
                        DocPicture pic = new DocPicture(doc);
                        pic.LoadImage(image);
                        pic.Width = tdWidth;
                        pic.Height = tdWidth * image.Height / image.Width;
                        range = selection.GetAsOneRange();
                        index = range.OwnerParagraph.ChildObjects.IndexOf(range);
                        range.OwnerParagraph.ChildObjects.Insert(index, pic);
                        range.OwnerParagraph.ChildObjects.Remove(range);
                    }
                }
                string strPath = Path.GetDirectoryName(newPath);
                if (!Directory.Exists(strPath))
                {
                    Directory.CreateDirectory(strPath);
                }
                doc.SaveToFile(newPath);
                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            finally
            {
                doc.Close();
            }
        }
    }
}
